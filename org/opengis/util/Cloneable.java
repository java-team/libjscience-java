package org.opengis.util;

public abstract interface Cloneable extends java.lang.Cloneable
{
  public abstract Object clone();
}

/* Location:           D:\Libraries\jscience-4.3\lib\geoapi.jar
 * Qualified Name:     org.opengis.util.Cloneable
 * JD-Core Version:    0.6.0
 */