package org.opengis.util;

import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

public abstract class CodeList
  implements Comparable, Serializable
{
  private static final long serialVersionUID = 5655809691319522885L;
  private final transient int ordinal;
  private final String name;

  protected CodeList(String paramString, Collection paramCollection)
  {
    name = (paramString = paramString.trim());
    synchronized (paramCollection)
    {
      ordinal = paramCollection.size();
      assert (!contains(paramCollection, paramString)) : paramString;
      if (!paramCollection.add(this))
        throw new IllegalArgumentException(String.valueOf(paramCollection));
    }
  }

  private static boolean contains(Collection paramCollection, String paramString)
  {
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext())
    {
      CodeList localCodeList = (CodeList)localIterator.next();
      if (paramString.equalsIgnoreCase(localCodeList.name))
        return true;
    }
    return false;
  }

  public final int ordinal()
  {
    return ordinal;
  }

  public final String name()
  {
    return name;
  }

  public abstract CodeList[] family();

  public final int compareTo(Object paramObject)
  {
    Class localClass1 = getClass();
    Class localClass2 = paramObject.getClass();
    if (!localClass1.equals(localClass2))
      throw new ClassCastException("Can't compare " + localClass1.getName() + " to " + localClass2.getName());
    return ordinal - ((CodeList)paramObject).ordinal;
  }

  public String toString()
  {
    String str = getClass().getName();
    int i = str.lastIndexOf('.');
    if (i >= 0)
      str = str.substring(i + 1);
    return str + '[' + name + ']';
  }

  protected Object readResolve()
    throws ObjectStreamException
  {
    CodeList[] arrayOfCodeList = family();
    for (int i = 0; i < arrayOfCodeList.length; i++)
    {
      assert (arrayOfCodeList[i].ordinal == i) : i;
      if (name.equals(arrayOfCodeList[i].name))
        return arrayOfCodeList[i];
    }
    throw new InvalidObjectException(toString());
  }
}

/* Location:           D:\Libraries\jscience-4.3\lib\geoapi.jar
 * Qualified Name:     org.opengis.util.CodeList
 * JD-Core Version:    0.6.0
 */