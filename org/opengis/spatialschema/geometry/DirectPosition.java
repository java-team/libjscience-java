package org.opengis.spatialschema.geometry;

import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.spatialschema.geometry.geometry.Position;
import org.opengis.util.Cloneable;

public abstract interface DirectPosition extends Position, Cloneable
{
  public abstract int getDimension();

  public abstract double[] getCoordinates();

  public abstract double getOrdinate(int paramInt)
    throws IndexOutOfBoundsException;

  public abstract void setOrdinate(int paramInt, double paramDouble)
    throws IndexOutOfBoundsException;

  public abstract CoordinateReferenceSystem getCoordinateReferenceSystem();

  public abstract Object clone();
}

/* Location:           D:\Libraries\jscience-4.3\lib\geoapi.jar
 * Qualified Name:     org.opengis.spatialschema.geometry.DirectPosition
 * JD-Core Version:    0.6.0
 */