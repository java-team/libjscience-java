package org.opengis.metadata;

import org.opengis.metadata.citation.Citation;

public abstract interface Identifier
{
  public static final String CODE_KEY = "code";
  public static final String AUTHORITY_KEY = "authority";
  public static final String VERSION_KEY = "version";

  public abstract String getCode();

  public abstract Citation getAuthority();

  public abstract String getVersion();
}

/* Location:           D:\Libraries\jscience-4.3\lib\geoapi.jar
 * Qualified Name:     org.opengis.metadata.Identifier
 * JD-Core Version:    0.6.0
 */