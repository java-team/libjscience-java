package org.opengis.referencing.crs;

import org.opengis.referencing.ReferenceSystem;
import org.opengis.referencing.cs.CoordinateSystem;

public abstract interface CoordinateReferenceSystem extends ReferenceSystem
{
  public abstract CoordinateSystem getCoordinateSystem();
}

/* Location:           D:\Libraries\jscience-4.3\lib\geoapi.jar
 * Qualified Name:     org.opengis.referencing.crs.CoordinateReferenceSystem
 * JD-Core Version:    0.6.0
 */