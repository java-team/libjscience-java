package org.opengis.referencing.cs;

import org.opengis.referencing.IdentifiedObject;

public abstract interface CoordinateSystem extends IdentifiedObject
{
  public abstract int getDimension();

  public abstract CoordinateSystemAxis getAxis(int paramInt)
    throws IndexOutOfBoundsException;
}

/* Location:           D:\Libraries\jscience-4.3\lib\geoapi.jar
 * Qualified Name:     org.opengis.referencing.cs.CoordinateSystem
 * JD-Core Version:    0.6.0
 */