package org.opengis.referencing.cs;

import java.util.ArrayList;
import java.util.List;
import org.opengis.util.CodeList;

public final class AxisDirection extends CodeList
{
  private static final long serialVersionUID = -4405275475770755714L;
  private static final List VALUES = new ArrayList(32);
  public static final AxisDirection OTHER = new AxisDirection("OTHER");
  public static final AxisDirection NORTH;
  public static final AxisDirection NORTH_NORTH_EAST;
  public static final AxisDirection NORTH_EAST;
  public static final AxisDirection EAST_NORTH_EAST;
  public static final AxisDirection EAST;
  public static final AxisDirection EAST_SOUTH_EAST;
  public static final AxisDirection SOUTH_EAST;
  public static final AxisDirection SOUTH_SOUTH_EAST;
  public static final AxisDirection SOUTH;
  public static final AxisDirection SOUTH_SOUTH_WEST;
  public static final AxisDirection SOUTH_WEST;
  public static final AxisDirection WEST_SOUTH_WEST;
  public static final AxisDirection WEST;
  public static final AxisDirection WEST_NORTH_WEST;
  public static final AxisDirection NORTH_WEST;
  public static final AxisDirection NORTH_NORTH_WEST;
  public static final AxisDirection UP;
  public static final AxisDirection DOWN;
  public static final AxisDirection GEOCENTRIC_X;
  public static final AxisDirection GEOCENTRIC_Y;
  public static final AxisDirection GEOCENTRIC_Z;
  public static final AxisDirection FUTURE;
  public static final AxisDirection PAST;
  public static final AxisDirection COLUMN_POSITIVE;
  public static final AxisDirection COLUMN_NEGATIVE;
  public static final AxisDirection ROW_POSITIVE;
  public static final AxisDirection ROW_NEGATIVE;
  public static final AxisDirection DISPLAY_RIGHT;
  public static final AxisDirection DISPLAY_LEFT;
  public static final AxisDirection DISPLAY_UP;
  public static final AxisDirection DISPLAY_DOWN;

  /** @deprecated */
  public static final AxisDirection RIGHT;

  /** @deprecated */
  public static final AxisDirection LEFT;

  /** @deprecated */
  public static final AxisDirection TOP;

  /** @deprecated */
  public static final AxisDirection BOTTOM;
  private transient AxisDirection opposite;

  public AxisDirection(String paramString)
  {
    super(paramString, VALUES);
  }

  private AxisDirection(String paramString, AxisDirection paramAxisDirection)
  {
    this(paramString);
    if (paramAxisDirection.opposite != null)
      throw new IllegalArgumentException(String.valueOf(paramAxisDirection));
    opposite = paramAxisDirection;
    paramAxisDirection.opposite = this;
  }

  public static AxisDirection[] values()
  {
    synchronized (VALUES)
    {
      return (AxisDirection[])(AxisDirection[])VALUES.toArray(new AxisDirection[VALUES.size()]);
    }
  }

  public CodeList[] family()
  {
    return values();
  }

  /** @deprecated */
  public AxisDirection inverse()
  {
    return opposite != null ? opposite : this;
  }

  public AxisDirection opposite()
  {
    return opposite;
  }

  public AxisDirection absolute()
  {
    AxisDirection localAxisDirection = opposite;
    if ((localAxisDirection != null) && (localAxisDirection.ordinal() < ordinal()))
      return localAxisDirection;
    return this;
  }

  static
  {
    OTHER.opposite = OTHER;
    NORTH = new AxisDirection("NORTH");
    NORTH_NORTH_EAST = new AxisDirection("NORTH_NORTH_EAST");
    NORTH_EAST = new AxisDirection("NORTH_EAST");
    EAST_NORTH_EAST = new AxisDirection("EAST_NORTH_EAST");
    EAST = new AxisDirection("EAST");
    EAST_SOUTH_EAST = new AxisDirection("EAST_SOUTH_EAST");
    SOUTH_EAST = new AxisDirection("SOUTH_EAST");
    SOUTH_SOUTH_EAST = new AxisDirection("SOUTH_SOUTH_EAST");
    SOUTH = new AxisDirection("SOUTH", NORTH);
    SOUTH_SOUTH_WEST = new AxisDirection("SOUTH_SOUTH_WEST", NORTH_NORTH_EAST);
    SOUTH_WEST = new AxisDirection("SOUTH_WEST", NORTH_EAST);
    WEST_SOUTH_WEST = new AxisDirection("WEST_SOUTH_WEST", EAST_NORTH_EAST);
    WEST = new AxisDirection("WEST", EAST);
    WEST_NORTH_WEST = new AxisDirection("WEST_NORTH_WEST", EAST_SOUTH_EAST);
    NORTH_WEST = new AxisDirection("NORTH_WEST", SOUTH_EAST);
    NORTH_NORTH_WEST = new AxisDirection("NORTH_NORTH_WEST", SOUTH_SOUTH_EAST);
    UP = new AxisDirection("UP");
    DOWN = new AxisDirection("DOWN", UP);
    GEOCENTRIC_X = new AxisDirection("GEOCENTRIC_X");
    GEOCENTRIC_Y = new AxisDirection("GEOCENTRIC_Y");
    GEOCENTRIC_Z = new AxisDirection("GEOCENTRIC_Z");
    FUTURE = new AxisDirection("FUTURE");
    PAST = new AxisDirection("PAST", FUTURE);
    COLUMN_POSITIVE = new AxisDirection("COLUMN_POSITIVE");
    COLUMN_NEGATIVE = new AxisDirection("COLUMN_NEGATIVE", COLUMN_POSITIVE);
    ROW_POSITIVE = new AxisDirection("ROW_POSITIVE");
    ROW_NEGATIVE = new AxisDirection("ROW_NEGATIVE", ROW_POSITIVE);
    DISPLAY_RIGHT = new AxisDirection("DISPLAY_RIGHT");
    DISPLAY_LEFT = new AxisDirection("DISPLAY_LEFT", DISPLAY_RIGHT);
    DISPLAY_UP = new AxisDirection("DISPLAY_UP");
    DISPLAY_DOWN = new AxisDirection("DISPLAY_DOWN", DISPLAY_UP);
    RIGHT = DISPLAY_RIGHT;
    LEFT = DISPLAY_LEFT;
    TOP = DISPLAY_UP;
    BOTTOM = DISPLAY_DOWN;
  }
}

/* Location:           D:\Libraries\jscience-4.3\lib\geoapi.jar
 * Qualified Name:     org.opengis.referencing.cs.AxisDirection
 * JD-Core Version:    0.6.0
 */