package org.opengis.referencing;

import java.util.Collection;
import java.util.Set;
import org.opengis.metadata.Identifier;
import org.opengis.util.InternationalString;

public abstract interface IdentifiedObject
{
  public static final String NAME_KEY = "name";
  public static final String ALIAS_KEY = "alias";
  public static final String IDENTIFIERS_KEY = "identifiers";
  public static final String REMARKS_KEY = "remarks";

  public abstract Identifier getName();

  public abstract Collection getAlias();

  public abstract Set getIdentifiers();

  public abstract InternationalString getRemarks();

  public abstract String toWKT()
    throws UnsupportedOperationException;
}

/* Location:           D:\Libraries\jscience-4.3\lib\geoapi.jar
 * Qualified Name:     org.opengis.referencing.IdentifiedObject
 * JD-Core Version:    0.6.0
 */