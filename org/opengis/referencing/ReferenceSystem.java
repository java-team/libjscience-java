package org.opengis.referencing;

import org.opengis.metadata.extent.Extent;
import org.opengis.util.InternationalString;

public abstract interface ReferenceSystem extends IdentifiedObject
{
  public static final String VALID_AREA_KEY = "validArea";
  public static final String SCOPE_KEY = "scope";

  public abstract Extent getValidArea();

  public abstract InternationalString getScope();
}

/* Location:           D:\Libraries\jscience-4.3\lib\geoapi.jar
 * Qualified Name:     org.opengis.referencing.ReferenceSystem
 * JD-Core Version:    0.6.0
 */